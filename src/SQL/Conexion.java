 /*
 based on https://youtu.be/nbAYB6HyTQI
 */
package SQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HECTOR
 */
public class Conexion {
//variable declarations

    private static Connection conn;
    private static final String DRIVER = "com.mysql.jdbc.Driver";//ya no hace falta 
    private static final String JDBC = "jdbc:mysql:";
    private static final String SERVER = "localhost:3306";
    private static final String BD = "school";
    private static final String URL = JDBC + "//" + SERVER + "/" + BD;
    private static final String USER = "root";
    private static final String PASSWD = "";

    public Conexion() { //constructor
        conn = null; //inicializar la variable de tipo Connection
        try {
            //  Class.forName(DRIVER); //EN DESUSO 
            //se carga en la var. conn el result del metodo getCon de la clase DrivM
            conn = DriverManager.getConnection(URL, USER, PASSWD);
            //verificamos que la conexión sea correcta
            if (conn != null) {
                System.out.println("Conexión ok, BD:" + conn.getCatalog());
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e);
        }
    }

    public static Connection conectar() { //retorna la conexión
        return conn;

    }

    public static void desconectar() {

        try {
            //        conn = null;
//        if (conn == null) {
//            System.out.println("conexión terminada");
//        }
            conn.close();
            if (conn.isClosed()) {
                System.out.println("conexión terminada");
            }
        } catch (SQLException ex) {
             System.out.println("error al desconectar "+ ex.getMessage());
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String args[]) {
        Conexion conexion = new Conexion();
        Connection conectar;
        conectar = Conexion.conectar();
        Conexion.desconectar();

    }

}
