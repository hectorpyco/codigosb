/*
 based on http://www.iuma.ulpgc.es/users/lhdez/inves/pfcs/memoria-ivan/node8.html#SECTION00843000000000000000
 */
package SQL;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HECTOR
 */
public class Iuma {

//    public static  String driver = "com.mysql.jdbc.Driver";
//    // public static final String url = "jdbc:mysql://localhost:3306/school";
//    public static  String url = "jdbc:mysql://db4free.net:3306/fcytunca"; //test usando el servicio online db4free
////    public static final String login = "root";
////    public static final String password = "";
//    public static  String login = "fcytunca";
//    public static  String password = "Unca2018";
    private String driver, url, login, password;

    Connection connection = null;
    Statement statement = null;

    public void leer() throws IOException {
        try {
            BufferedReader br = new BufferedReader(
                    new FileReader("src/SQL/files/server.txt"));
            driver = br.readLine();
            url = br.readLine();
            login = br.readLine();
            password = br.readLine();

        } catch (FileNotFoundException ex) {
            System.out.println("error al leer el archivo: "+ ex.getMessage());
            Logger.getLogger(Iuma.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initialize() throws
            SQLException, ClassNotFoundException, IOException {
        leer();

        //    Class.forName(driver); en desuso
        connection = DriverManager.getConnection(url,
                login,
                password);
        if (connection != null) {
            System.out.println("Conexión ok, BD:" + connection.getCatalog());
        }

    }

    public void crearTabla() {
        String createTableBank
                = "CREATE TABLE BANK ("
                + "id INT(8) NOT NULL AUTO_INCREMENT, "
                + "client VARCHAR(100) NOT NULL, "
                + "password VARCHAR(20) NOT NULL, "
                + "balance Integer NOT NULL, "
                + "PRIMARY KEY(id))";

        try {
            System.out.println(createTableBank);
            statement = connection.createStatement();
            statement.executeUpdate(createTableBank);
            System.out.println(statement.getResultSet());
        } catch (SQLException ex) {
            Logger.getLogger(Iuma.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("SQLException: " + ex);
        }

    }

    public void close() throws SQLException {

        try {
            connection.close();
        } catch (SQLException e) {
            throw e;
        }
        if (connection.isClosed()) {
            System.out.println("conexión terminada");
        }

    }

    public void insertData() throws SQLException, IOException {

        String client, password_cliente, balance;
        statement = connection.createStatement();
        try (BufferedReader br = new BufferedReader(
                new FileReader("src/SQL/files/clients.txt"))) {
            do {
                client = br.readLine();
                password_cliente = br.readLine();
                balance = br.readLine();
                String sqlString
                        = "INSERT INTO BANK (client,password,balance) VALUES('"
                        + client + "','" + password_cliente + "',"
                        + balance + ")";
                System.out.println(sqlString);
                statement.executeUpdate(sqlString);
                System.out.println(statement.getUpdateCount());

            } while (br.readLine() != null);
        } catch (IOException e) {
            System.out.println("error al insertar: " + e);
        } finally {
            statement.close();
        }

    }

    public void queryAll() throws SQLException {

        String sqlString
                = "SELECT client, password, balance"
                + " FROM BANK";
        System.out.println(sqlString);
        statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(sqlString);

        while (rs.next()) {
            System.out.println(rs.getString("client")
                    + rs.getString("password")
                    + rs.getInt("balance"));
        }
    }

    public static void main(String args[]) throws IOException {

        Iuma nuevo = new Iuma();
        try {
            nuevo.initialize();
            //nuevo.crearTabla(); //crear la tabla
            //nuevo.insertData();  //insertar datos desde un archivo
            nuevo.queryAll();
            nuevo.close();
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println("error en el main: " + ex);
            Logger.getLogger(Iuma.class.getName()).log(Level.SEVERE, null, ex);
            
        }

    }

}
