/*
 based on: https://youtu.be/5vzCjvUwMXg
7 Steps to Connect Application to database
1)Import the package
2)Load and Register driver
3)Create Connection
4)Create Statement
5)Execute the query
6)process the results
7)close connection

 */
package SQL;

import java.sql.*;

/**
 *
 * @author HECTOR
 */
public class Telusko {

    public static void main(String[] args) throws Exception {
       String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/school";
        String user = "root";
        String pwd = "";
        String query = "Select * from user ";

        //2)Load and Register driver
        Class.forName(driver);
        //3)Create Connection
        Connection con = DriverManager.getConnection(url, user, pwd);
        //4)Create Statement
        Statement st = con.createStatement();
        //5)Execute the query
        ResultSet rs = st.executeQuery(query);
        //6)process the results

        while (rs.next()) {
            String name = rs.getString("username");
            int id = rs.getInt(5);
            System.out.println(name);
            System.out.println(id);
            Object next = rs.next();
        }

    }

}
