-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 30-10-2018 a las 18:27:09
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `school`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(16) NOT NULL,
  `married` tinyint(1) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`username`, `married`, `email`, `password`, `create_time`, `id`) VALUES
('josess', 1, 'jose@jose.com', 'jose123', '2018-10-15 18:55:13', 8),
('pablo', 0, 'papap', 'pepe', '2018-10-15 19:14:07', 9),
('jair', 1, 'jair@sfj.com', 'jair', '2018-10-15 19:17:36', 10),
('saf', 1, 'gfg', 'gfsd', '2018-10-15 19:46:44', 12),
('siodsaonf', 1, 'askfa', 'nskfdn', '2018-10-15 20:27:28', 13),
('', 0, '', '', '2018-10-15 20:29:26', 14),
('hectorR', 1, 'sdffoiash', 'sojdoda', '2018-10-16 19:12:39', 15),
('klsankq', 0, 'SADFASD', 'LSNSKND', '2018-10-16 19:13:33', 16),
('kobe', 1, 'soja', 'jsafihdi', '2018-10-19 12:42:32', 17);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
