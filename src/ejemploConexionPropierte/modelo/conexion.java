/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploConexionPropierte.modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HECTOR
 */
public class conexion {
    private static Connection conn;
//    private static final String DRIVER = "com.mysql.jdbc.Driver";//ya no hace falta 
////    private static final String BD = "school";
//    private static final String URL = "jdbc:mysql://localhost/school";
//    private static final String USER = "root";
//    private static final String PASSWD = "";

    public conexion() {
        try {
            Properties miProp= new miPropiedad().getProperties();
            conn = DriverManager.getConnection(miProp.getProperty("url")+"/"+miProp.getProperty("bd"),
                    miProp.getProperty("USER"), miProp.getProperty("PASSWD"));
            
            if (conn != null) {
                System.out.println("Conexión a base de datos OK");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Connection getConn() {
        return conn;
    }
    
    
   

}
