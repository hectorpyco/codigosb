/*
 based on video 7.1.3 from (Video2Brain) Java 7
 */
package myApp;

import SQL.Conexion;
import com.mysql.jdbc.DatabaseMetaData;
import com.sun.xml.internal.ws.api.addressing.WSEndpointReference.Metadata;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author HECTOR
 */
public class SimpleConexion {

    private String url, login, pwd;
    int res = 0;
    Connection connection = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet resultado = null;

    public int getRes() {
        return res;
    }

    public void setRes(int res) {
        this.res = res;
    }

    public ResultSet getResultado() {
        return resultado;
    }

    public void setResultado(ResultSet resultado) {
        this.resultado = resultado;
    }
    

    public static void main(String[] args) {
        //solo para pruebas

        SimpleConexion con = new SimpleConexion();

        con.obtenerColumnas("select * from user");

    }

    public void leerDatos() throws IOException {
        try {
            BufferedReader lector = new BufferedReader(new FileReader("src/SQL/files/server1.txt"));
            url = lector.readLine();
            login = lector.readLine();
            pwd = lector.readLine();

        } catch (FileNotFoundException ex) {
            System.out.println("error al leer el archivo " + ex.getMessage());

        } catch (IOException io) {
            System.out.println("error al leer: " + io.getMessage());
        }
    }

    public void conectar() {

        try {
            leerDatos();
            connection = DriverManager.getConnection(url, login, pwd);
            if (connection.isValid(5000)) {
                System.out.println("Conexión exitosa a la BD " + connection.getCatalog());
            }
        } catch (SQLException ex) {
            System.out.println("error sql al conectar " + ex.getMessage());

        } catch (IOException ex) {
            System.out.println("error IO al conectar " + ex.getMessage());
        }

    }

    public void desconectar() {
        try {
            connection.close();
            if (connection.isClosed()) {
                System.out.println("desconexión exitosa ");
            }
        } catch (SQLException ex) {
            System.out.println("error al desconectar " + ex.getMessage());
        }
    }

    public ResultSet buscarporID(String id, String consulta) {
        //este método recibe consultas de tipo 
        // select * from tabla where campo =?
        // donde tabla y campo son parámetros
        System.out.println("buscarporID");
        conectar();
        try {

            ps = connection.prepareStatement(consulta);
            ps.setString(1, id);
            System.out.println(ps.toString());
            resultado = ps.executeQuery();

        } catch (SQLException ex) {
            System.out.println("error al buscar " + ex.getMessage());
        }
        //desconectar();
        return resultado;

    }

    public int borrarDatos(String id) {
        System.out.println("borrarDatos");
        String borrar = "DELETE from school.user where id =" + id;

        try {
            statement = connection.createStatement();
            res = statement.executeUpdate(borrar);

        } catch (SQLException ex) {
            System.out.println("error al borrar " + ex.getMessage());
        }
        return res;
    }

    public int insertarDatos(String USERNAME, int MARRIED, String email, String password) {
        String inserta = "INSERT INTO SCHOOL.user (USERNAME, MARRIED,email,password)"
                + " VALUES (?,?,?,?)";
        try {
            ps = connection.prepareStatement(inserta);
            ps.setString(1, USERNAME);
            ps.setInt(2, MARRIED);
            ps.setString(3, email);
            ps.setString(4, password);
            System.out.println(ps.toString());
            res = ps.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("error al insertar " + ex.getMessage());
        }
        return res;

    }

    public int actualizaDatos(String USERNAME, int MARRIED, String email, String password, String id) {
        System.out.println("actualizaDatos");
        String actualiza = "update school.user set username=?,"
                + "married = ?, email=?"
                + ",password=? where id =?";
        try {
            ps = connection.prepareStatement(actualiza);
            ps.setString(1, USERNAME);
            ps.setInt(2, MARRIED);
            ps.setString(3, email);
            ps.setString(4, password);
            ps.setString(5, id);
            res = ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("error al actualizar " + ex.getMessage());
        }
        return res;
    }

//método para traer los datos de la BD, devuelve un objeto DFtblmodel o heredado
    public MimodeloTabla cargarTabla(String consulta) {
        System.out.println("cargarTabla con: " + consulta);
        Object[] columnas=null;
        //String sql = consulta;
        MimodeloTabla dm = new MimodeloTabla();
        //atención: MiModeloTabla pone como boolean la segunda columna

        //trae los nombres de columnas de la consulta sql y los pone como
        //nombres de columna de la tabla
        try {
            columnas = obtenerColumnas(consulta);
            dm.setColumnIdentifiers(columnas);
        } catch (Exception e) {
            System.err.println("error al agregar columnas " + e);
        }
        //crea un objeto por fila 
        try {
            //resultado fue cargado en el anterior método
            while (resultado.next()) {//si existen datos cargar el modelo
                // Se crea un array que será una de las filas de la tabla. 
                // que tendrá la cantidad de columnas calculada
                Object[] fila = new Object[columnas.length]; 

                // Se rellena cada posición del array con una de las columnas de la tabla en base de datos.
                for (int i = 0; i < columnas.length; i++) {
                // El primer indice en rs es el 1, no el cero, por eso se suma 1.
                    fila[i] = resultado.getObject(i + 1); 
                    System.out.println("fila: " + fila[i]);
                }
                // Se añade al modelo la fila completa.
                dm.addRow(fila);
            }
            desconectar();
            return dm;

        } catch (Exception e) {
           System.err.println("error al agregar filas " + e);
            desconectar();
            return null;
        }

    }
//método para obtener las cabeceras de una consulta
    public Object[] obtenerColumnas(String consulta) {
        System.out.println("obtenerColumnas "+ consulta);

        //traer las columnas del resultset, basado en:
        //http://www.chuidiang.org/java/mysql/resultset_jtable.php
        String catalogo = null, tabla = null;
        try {
            conectar();
            ps = connection.prepareStatement(consulta);
            resultado = ps.executeQuery();
            ResultSetMetaData metaDatos = resultado.getMetaData();
            catalogo = metaDatos.getCatalogName(1);
            tabla = metaDatos.getTableName(1);
            System.out.println("catalogo " + catalogo + ", tabla: " + tabla);
            // Se obtiene el número de columnas.
            int numeroColumnas = metaDatos.getColumnCount();
            System.out.println("numero columnas: " + numeroColumnas);

            // Se crea un array de etiquetas para rellenar
            Object[] etiquetas = new Object[numeroColumnas];

            // Se obtiene cada una de las etiquetas para cada columna
            for (int i = 0; i < numeroColumnas; i++) {
                // Nuevamente, para ResultSetMetaData la primera columna es la 1. 
                etiquetas[i] = metaDatos.getColumnLabel(i + 1);
                System.out.println("etiqueta: " + etiquetas[i]);
            }

           
            return etiquetas;
        } catch (SQLException ex) {
            System.err.println("error al obtener metaDatos: " + ex);
            return null;
        }

    }

    public class MimodeloTabla extends DefaultTableModel {

        //basado en http://chuwiki.chuidiang.org/index.php?title=JTable
        /**
         * Sobreescribimos el método getColumnClass que devuelve la clase, para
         * que en la cuarta columna esta sea de tipo Boolean.class y en la tabla
         * aparezca como checkbox una columna Boolean, el resto object
         */
        @Override
        public Class getColumnClass(int column) {
            if (column == 1) { 
//atención: este índice debe coincidir con el valor boolean de la consulta
               // System.out.println("metodo getcolumnclass sobreescrito, columna ==2");
                return Boolean.class;

            }

            return Object.class;
        }
    }

}
