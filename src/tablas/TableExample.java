/*
based on: https://www.codejava.net/java-se/swing/a-simple-jtable-example-for-display
 */
package tablas;

import myApp.SimpleConexion;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

/**
 *
 * @author HECTOR
 */
public class TableExample extends JFrame {

    SimpleConexion con = new SimpleConexion();

    public TableExample() throws SQLException {
        //headers for the table
        String[] columns;
        columns = new String[]{
            "Id", "Name", "Hourly Rate", "Part Time", "Country"
        };

        
        //actual data for the table in a 2d array
        Object[][] data = new Object[][]{
            {1, "John", 40.0, false, "USA"},
            {2, "Rambo", 70.0, false, "FR"},
            {3, "Zorro", 60.0, true, "PY"},};

        //create table with data
        JTable table = new JTable(data, columns);

        //add the table to the frame
        this.add(new JScrollPane(table));

        this.setTitle("Table Example");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    new TableExample();
                } catch (SQLException ex) {
                    Logger.getLogger(TableExample.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

}
