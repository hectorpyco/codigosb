/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablas.cancino;

import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author HECTOR
 */
public class Panel extends JPanel {

    public Panel() {
        String[] cabeceras = {
            "ID", "NOMBRE", "EDAD"
        };
        Object[][] datos = {
            {
                "1", "ALE", "40"
            },
            {
                "2", "juan", "43"
            },};
        //tabla sin modelo        
        //usamos el tercer constructor: JTable(Object[][] rowData,Object[] columnNames)
        JTable tabla1 = new JTable(datos, cabeceras);
        tabla1.setPreferredScrollableViewportSize(new Dimension(250, 50));
        //metemos la tabla en un ScrollPane: https://docs.oracle.com/javase/tutorial/uiswing/components/scrollpane.html
        JScrollPane myScroll1 = new JScrollPane(tabla1);
        add(myScroll1);

        //tabla con modelo
        DefaultTableModel modelo1 = new DefaultTableModel(datos, cabeceras);
        //usamos el constructor JTable(TableModel dm)
        JTable tabla2 = new JTable(modelo1);
        tabla2.setPreferredScrollableViewportSize(new Dimension(500, 100));
        JScrollPane myScroll2 = new JScrollPane(tabla2);
        add(myScroll2);

        //tabla dinámica
        DefaultTableModel modelo2 = new DefaultTableModel();
        /*      modelo2.addColumn(cabeceras);
        modelo2.addRow(datos);
         */
        modelo2.addColumn("ID");
        modelo2.addColumn("name");
        modelo2.addColumn("edad");

        //creamos los datos desde un arreglo con la clase Personas
        ArrayList<Personas> personas = new ArrayList<Personas>();
        for (int i = 0; i < 10; i++) {
            //generamos los datos con ayuda de i
            personas.add(new Personas("nombre_" + i, i, 10 + i));
        }
        //cargamos los datos con un bucle foreach
        for (Personas p : personas) {
            //creamos el Object para cargar las filas
            Object[] columna = new Object[3]; //tenemos 3 columnas
            columna[0] = p.getID();
            columna[1] = p.getNombre();
            columna[2] = p.getEdad();
            modelo2.addRow(columna);
        }

        JTable tabla3 = new JTable(modelo2);
        tabla3.setPreferredScrollableViewportSize(new Dimension(900, 150));

        //reaccionar a eventos
        tabla3.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); //seleccionar una fila a la vez
        tabla3.getSelectionModel().addListSelectionListener( //agregar un listener  con evento
                new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int filas = tabla3.getSelectedRowCount(); //deberia tomar siempre solo una fila.
                if (filas == 1) { //verificamos que efectivamente se selecciona una sola fila
                    int filaSel = tabla3.getSelectedRow(); //guardamos la fila seleccionada en una variable

                    Object nombreSel = tabla3.getValueAt(filaSel, 1); //capturamos el nombre que está en columna 1.

                    JOptionPane.showMessageDialog(null, "fila: " + filaSel + ", "
                            + ", nombre: " + nombreSel.toString()
                            + ", edad: " + tabla3.getValueAt(filaSel, 2));//mostramos directament el valor de edad en columna 2
                }

            }
        }
        );

        JScrollPane myScroll3 = new JScrollPane(tabla3);
        add(myScroll3);
        //add(tabla3);

    }

}
