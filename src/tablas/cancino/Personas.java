/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablas.cancino;

/**
 *
 * @author HECTOR
 */
public class Personas {
    private String nombre;
    private int ID, edad;    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Personas(String nombre, int ID, int edad) {
        this.nombre = nombre;
        this.ID = ID;
        this.edad = edad;
    }
}
